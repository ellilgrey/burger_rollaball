﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstacleController : MonoBehaviour
{
    public float speed = .1f;
    public float maxHeight = 1f;
    public float minHeight = .5f;

    private Vector3 offset = new Vector3(0, 1, 0);
    private string direction;

    // Start is called before the first frame update
    void Start()
    {
        offset.y *= speed;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y > maxHeight)
        {
            direction = "down";
        }else if(transform.position.y < minHeight)
        {
            direction = "up";
        }
    }

    private void FixedUpdate()
    {
        if(direction == "up")
        {
            transform.position = transform.position + offset;
        }else
        {
            transform.position = transform.position - offset;
        }
    }
}
