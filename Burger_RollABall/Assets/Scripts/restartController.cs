﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class restartController : MonoBehaviour
{
    // Start is called before the first frame update
    public Text gameOverText;
    public Text winText;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if( gameOverText.text == "Game Over Press [Enter] to Restart" || winText.text == "You Win!")
        {
            if(Input.GetButtonDown("Submit"))
            {
                RestartLevel();
            }
        }
    }

    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
