﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playerController : MonoBehaviour
{
    //variables
    public float speed;
    public Text countText;
    public Text winText;
    public Text gameOverText;
    public float jumpForce;

    private Rigidbody rb;

    private int count;
    private bool onGround;

    private void Awake()
    {
        gameObject.SetActive(true);
    }
    // Start is called before the first frame update
    void Start()
    {
        //sets rb as the RigidBody component in Unity
        rb = GetComponent<Rigidbody>();

        //set the count value to 0 at start
        count = 0;

        //calls a function to set the count text
        SetCountText();

        //sets the win text to blank
        winText.text = "";

        //sets the game over text to blank
        gameOverText.text = "";
    }

    // Update is called once per frame (use for everything except for physics)
    void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);
    }

    // use FixedUpdate() for physics processes
    private void FixedUpdate()
    {
        //sets directions
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        //creates the movement vector
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        //adds the force in the direction of Vector3 at speed
        rb.AddForce(movement * speed);

        if(Input.GetButton("Jump"))
        {
            Jump();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //if the player collides with a pick up, deactivate the pick up and raise the count text
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }

        //if the player collides with an obstacle, deactivate the player
        if(other.gameObject.CompareTag("obstacle"))
        {
            other.gameObject.SetActive(false);

            if(winText.text == "")
            {
                gameOverText.text = "Game Over " +
                "Press [Enter] to Restart";
            }
            gameObject.SetActive(false);   
        }
    }

    //sets the count text to whatever count is
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win!";
        }
    }

    void Jump()
    {
        if(onGround)
        {
            print("jump");
            rb.AddForce(new Vector3(0,jumpForce,0));
        }
    }
}
